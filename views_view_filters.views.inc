<?php
/**
 * @file
 * Views integration file.
 */

/**
 * Implements hook_views_plugins().
 */
function views_view_filters_views_plugins() {
  return array(
    'display_extender' => array(
      'title' => t('Views-provided filter option values'),
      'help' => t('Extend filters to generate value options (including exposed filters) using a view.'),
      'handler' => 'views_view_filter_extender',
      'enabled' => TRUE,
    ),
  );
}
